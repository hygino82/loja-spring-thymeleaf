package br.dev.hygino.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PrincipalControle {

	@GetMapping("administrativo")
	public String acessarPrincipal() {
		return "administrativo/home";
	}
	
	@GetMapping("administrativo/funcionarios/cadastar")
	public String cadastrar() {
		return "administrativo/usuarios/cadastro";
	}
}
