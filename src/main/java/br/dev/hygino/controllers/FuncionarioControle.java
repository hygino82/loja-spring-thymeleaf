package br.dev.hygino.controllers;

import javax.validation.Valid;

import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import br.dev.hygino.entities.Funcionario;
import br.dev.hygino.repositories.FuncionarioRepository;

@RestController
@RequestMapping("/administrativo/funcionarios")
public class FuncionarioControle {

	private final FuncionarioRepository funcionarioRepository;

	FuncionarioControle(FuncionarioRepository funcionarioRepository) {
		this.funcionarioRepository = funcionarioRepository;
	}

	@GetMapping("/cadastrar")
	public ModelAndView cadastrar(Funcionario funcionario) {
		ModelAndView mv = new ModelAndView("administrativo/funcionarios/cadastro");
		mv.addObject("funcionario", funcionario);
		return mv;
	}

	@GetMapping("/listar")
	public String listar() {
		return "administrativo/funcionarios/lista";
	}
	
	@PostMapping("/salvar")
	public ModelAndView salvar(@Valid Funcionario funcionario,BindingResult result) {
		
		if(result.hasErrors()) {
			return cadastrar(funcionario);
		}
		funcionarioRepository.save(funcionario);
		
		return cadastrar(new Funcionario());
	}
}
